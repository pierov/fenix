/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package org.mozilla.fenix.home.sessioncontrol.viewholders

import android.graphics.Typeface
import android.text.SpannableString
import android.text.Spanned
import android.text.style.StyleSpan
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.tor_info_banner.view.*
import org.mozilla.fenix.R
import org.mozilla.fenix.home.sessioncontrol.TabSessionInteractor

class TorInfoBannerViewHolder(
    view: View,
    private val interactor: TabSessionInteractor
) : RecyclerView.ViewHolder(view) {

    init {
        with(view.info_banner_launch_button) {
            setOnClickListener {
                interactor.onTorInfoBannerLaunchClicked()
            }
        }

        with(view.info_banner_description) {
            val spannedString: SpannableString = SpannableString(text)
            spannedString.setSpan(StyleSpan(Typeface.BOLD), 120, 138,
                                  Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            setText(spannedString)
        }
    }

    companion object {
        const val LAYOUT_ID = R.layout.tor_info_banner
    }
}
