/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package org.mozilla.fenix.home.sessioncontrol.viewholders.onboarding

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.tor_onboarding_security_level.view.*
import org.mozilla.fenix.R
import org.mozilla.fenix.ext.components
import org.mozilla.fenix.home.sessioncontrol.OnboardingInteractor
import org.mozilla.fenix.onboarding.OnboardingRadioButton
import org.mozilla.fenix.tor.SecurityLevel
import org.mozilla.fenix.tor.SecurityLevelUtil
import org.mozilla.fenix.utils.view.addToRadioGroup

class TorOnboardingSecurityLevelViewHolder(
    view: View,
    private val interactor: OnboardingInteractor
) : RecyclerView.ViewHolder(view) {

    private var standardSecurityLevel: OnboardingRadioButton
    private var saferSecurityLevel: OnboardingRadioButton
    private var safestSecurityLevel: OnboardingRadioButton

    init {
        view.header_text.setOnboardingIcon(R.drawable.ic_onboarding_tracking_protection)

        standardSecurityLevel = view.security_level_standard_option
        saferSecurityLevel = view.security_level_safer_option
        safestSecurityLevel = view.security_level_safest_option

        view.description_text.text = view.context.getString(
            R.string.tor_onboarding_security_level_description
        )

        view.open_settings_button.setOnClickListener {
            interactor.onOpenSecurityLevelSettingsClicked()
        }

        setupRadioGroup(view)
    }

    private fun setupRadioGroup(view: View) {

        addToRadioGroup(standardSecurityLevel, saferSecurityLevel, safestSecurityLevel)

        val securityLevel = try {
            SecurityLevelUtil.getSecurityLevelFromInt(
                view.context.components.core.engine.settings.torSecurityLevel
            )
        } catch (e: IllegalStateException) {
            SecurityLevel.STANDARD
        }

        standardSecurityLevel.isChecked = securityLevel == SecurityLevel.STANDARD
        safestSecurityLevel.isChecked = securityLevel == SecurityLevel.SAFEST
        saferSecurityLevel.isChecked = securityLevel == SecurityLevel.SAFER

        standardSecurityLevel.onClickListener {
            updateSecurityLevel(SecurityLevel.STANDARD)
        }

        saferSecurityLevel.onClickListener {
            updateSecurityLevel(SecurityLevel.SAFER)
        }

        safestSecurityLevel.onClickListener {
            updateSecurityLevel(SecurityLevel.SAFEST)
        }

        updateSecurityLevel(securityLevel)
    }

    private fun updateSecurityLevel(newLevel: SecurityLevel) {
        val resources = itemView.context.resources
        val securityLevel = when (newLevel) {
            SecurityLevel.STANDARD -> resources.getString(R.string.tor_security_level_standard_option)
            SecurityLevel.SAFER -> resources.getString(R.string.tor_security_level_safer_option)
            SecurityLevel.SAFEST -> resources.getString(R.string.tor_security_level_safest_option)
        }
        itemView.current_level.text = resources.getString(
            R.string.tor_onboarding_chosen_security_level_label, securityLevel
        )
        itemView.context.components.let {
            it.core.engine.settings.torSecurityLevel = newLevel.intRepresentation
        }
    }

    companion object {
        const val LAYOUT_ID = R.layout.tor_onboarding_security_level
    }
}
